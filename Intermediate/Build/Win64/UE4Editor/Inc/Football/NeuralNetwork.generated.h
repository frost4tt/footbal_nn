// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FOOTBALL_NeuralNetwork_generated_h
#error "NeuralNetwork.generated.h already included, missing '#pragma once' in NeuralNetwork.h"
#endif
#define FOOTBALL_NeuralNetwork_generated_h

#define Football_Source_Football_NeuralNetwork_h_15_SPARSE_DATA
#define Football_Source_Football_NeuralNetwork_h_15_RPC_WRAPPERS
#define Football_Source_Football_NeuralNetwork_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Football_Source_Football_NeuralNetwork_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNeuralNetwork(); \
	friend struct Z_Construct_UClass_UNeuralNetwork_Statics; \
public: \
	DECLARE_CLASS(UNeuralNetwork, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Football"), NO_API) \
	DECLARE_SERIALIZER(UNeuralNetwork)


#define Football_Source_Football_NeuralNetwork_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUNeuralNetwork(); \
	friend struct Z_Construct_UClass_UNeuralNetwork_Statics; \
public: \
	DECLARE_CLASS(UNeuralNetwork, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Football"), NO_API) \
	DECLARE_SERIALIZER(UNeuralNetwork)


#define Football_Source_Football_NeuralNetwork_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNeuralNetwork(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNeuralNetwork) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNeuralNetwork); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNeuralNetwork); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNeuralNetwork(UNeuralNetwork&&); \
	NO_API UNeuralNetwork(const UNeuralNetwork&); \
public:


#define Football_Source_Football_NeuralNetwork_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNeuralNetwork(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNeuralNetwork(UNeuralNetwork&&); \
	NO_API UNeuralNetwork(const UNeuralNetwork&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNeuralNetwork); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNeuralNetwork); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNeuralNetwork)


#define Football_Source_Football_NeuralNetwork_h_15_PRIVATE_PROPERTY_OFFSET
#define Football_Source_Football_NeuralNetwork_h_12_PROLOG
#define Football_Source_Football_NeuralNetwork_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Football_Source_Football_NeuralNetwork_h_15_PRIVATE_PROPERTY_OFFSET \
	Football_Source_Football_NeuralNetwork_h_15_SPARSE_DATA \
	Football_Source_Football_NeuralNetwork_h_15_RPC_WRAPPERS \
	Football_Source_Football_NeuralNetwork_h_15_INCLASS \
	Football_Source_Football_NeuralNetwork_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Football_Source_Football_NeuralNetwork_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Football_Source_Football_NeuralNetwork_h_15_PRIVATE_PROPERTY_OFFSET \
	Football_Source_Football_NeuralNetwork_h_15_SPARSE_DATA \
	Football_Source_Football_NeuralNetwork_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Football_Source_Football_NeuralNetwork_h_15_INCLASS_NO_PURE_DECLS \
	Football_Source_Football_NeuralNetwork_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FOOTBALL_API UClass* StaticClass<class UNeuralNetwork>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Football_Source_Football_NeuralNetwork_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
