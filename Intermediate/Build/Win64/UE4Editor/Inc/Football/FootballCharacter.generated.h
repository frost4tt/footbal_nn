// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FOOTBALL_FootballCharacter_generated_h
#error "FootballCharacter.generated.h already included, missing '#pragma once' in FootballCharacter.h"
#endif
#define FOOTBALL_FootballCharacter_generated_h

#define Football_Source_Football_FootballCharacter_h_12_SPARSE_DATA
#define Football_Source_Football_FootballCharacter_h_12_RPC_WRAPPERS
#define Football_Source_Football_FootballCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Football_Source_Football_FootballCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFootballCharacter(); \
	friend struct Z_Construct_UClass_AFootballCharacter_Statics; \
public: \
	DECLARE_CLASS(AFootballCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Football"), NO_API) \
	DECLARE_SERIALIZER(AFootballCharacter)


#define Football_Source_Football_FootballCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFootballCharacter(); \
	friend struct Z_Construct_UClass_AFootballCharacter_Statics; \
public: \
	DECLARE_CLASS(AFootballCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Football"), NO_API) \
	DECLARE_SERIALIZER(AFootballCharacter)


#define Football_Source_Football_FootballCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFootballCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFootballCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFootballCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFootballCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFootballCharacter(AFootballCharacter&&); \
	NO_API AFootballCharacter(const AFootballCharacter&); \
public:


#define Football_Source_Football_FootballCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFootballCharacter(AFootballCharacter&&); \
	NO_API AFootballCharacter(const AFootballCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFootballCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFootballCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFootballCharacter)


#define Football_Source_Football_FootballCharacter_h_12_PRIVATE_PROPERTY_OFFSET
#define Football_Source_Football_FootballCharacter_h_9_PROLOG
#define Football_Source_Football_FootballCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Football_Source_Football_FootballCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Football_Source_Football_FootballCharacter_h_12_SPARSE_DATA \
	Football_Source_Football_FootballCharacter_h_12_RPC_WRAPPERS \
	Football_Source_Football_FootballCharacter_h_12_INCLASS \
	Football_Source_Football_FootballCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Football_Source_Football_FootballCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Football_Source_Football_FootballCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Football_Source_Football_FootballCharacter_h_12_SPARSE_DATA \
	Football_Source_Football_FootballCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Football_Source_Football_FootballCharacter_h_12_INCLASS_NO_PURE_DECLS \
	Football_Source_Football_FootballCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FOOTBALL_API UClass* StaticClass<class AFootballCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Football_Source_Football_FootballCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
