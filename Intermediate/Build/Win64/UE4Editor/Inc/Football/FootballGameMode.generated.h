// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FOOTBALL_FootballGameMode_generated_h
#error "FootballGameMode.generated.h already included, missing '#pragma once' in FootballGameMode.h"
#endif
#define FOOTBALL_FootballGameMode_generated_h

#define Football_Source_Football_FootballGameMode_h_12_SPARSE_DATA
#define Football_Source_Football_FootballGameMode_h_12_RPC_WRAPPERS
#define Football_Source_Football_FootballGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Football_Source_Football_FootballGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFootballGameMode(); \
	friend struct Z_Construct_UClass_AFootballGameMode_Statics; \
public: \
	DECLARE_CLASS(AFootballGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Football"), FOOTBALL_API) \
	DECLARE_SERIALIZER(AFootballGameMode)


#define Football_Source_Football_FootballGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFootballGameMode(); \
	friend struct Z_Construct_UClass_AFootballGameMode_Statics; \
public: \
	DECLARE_CLASS(AFootballGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Football"), FOOTBALL_API) \
	DECLARE_SERIALIZER(AFootballGameMode)


#define Football_Source_Football_FootballGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FOOTBALL_API AFootballGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFootballGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FOOTBALL_API, AFootballGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFootballGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FOOTBALL_API AFootballGameMode(AFootballGameMode&&); \
	FOOTBALL_API AFootballGameMode(const AFootballGameMode&); \
public:


#define Football_Source_Football_FootballGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FOOTBALL_API AFootballGameMode(AFootballGameMode&&); \
	FOOTBALL_API AFootballGameMode(const AFootballGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FOOTBALL_API, AFootballGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFootballGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFootballGameMode)


#define Football_Source_Football_FootballGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Football_Source_Football_FootballGameMode_h_9_PROLOG
#define Football_Source_Football_FootballGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Football_Source_Football_FootballGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Football_Source_Football_FootballGameMode_h_12_SPARSE_DATA \
	Football_Source_Football_FootballGameMode_h_12_RPC_WRAPPERS \
	Football_Source_Football_FootballGameMode_h_12_INCLASS \
	Football_Source_Football_FootballGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Football_Source_Football_FootballGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Football_Source_Football_FootballGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Football_Source_Football_FootballGameMode_h_12_SPARSE_DATA \
	Football_Source_Football_FootballGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Football_Source_Football_FootballGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Football_Source_Football_FootballGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FOOTBALL_API UClass* StaticClass<class AFootballGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Football_Source_Football_FootballGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
