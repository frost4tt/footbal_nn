// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Football/FootballGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFootballGameMode() {}
// Cross Module References
	FOOTBALL_API UClass* Z_Construct_UClass_AFootballGameMode_NoRegister();
	FOOTBALL_API UClass* Z_Construct_UClass_AFootballGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Football();
// End Cross Module References
	void AFootballGameMode::StaticRegisterNativesAFootballGameMode()
	{
	}
	UClass* Z_Construct_UClass_AFootballGameMode_NoRegister()
	{
		return AFootballGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AFootballGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFootballGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Football,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFootballGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "FootballGameMode.h" },
		{ "ModuleRelativePath", "FootballGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFootballGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFootballGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFootballGameMode_Statics::ClassParams = {
		&AFootballGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AFootballGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFootballGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFootballGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFootballGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFootballGameMode, 997885169);
	template<> FOOTBALL_API UClass* StaticClass<AFootballGameMode>()
	{
		return AFootballGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFootballGameMode(Z_Construct_UClass_AFootballGameMode, &AFootballGameMode::StaticClass, TEXT("/Script/Football"), TEXT("AFootballGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFootballGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
