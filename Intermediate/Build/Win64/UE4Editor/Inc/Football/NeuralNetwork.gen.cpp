// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Football/NeuralNetwork.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNeuralNetwork() {}
// Cross Module References
	FOOTBALL_API UClass* Z_Construct_UClass_UNeuralNetwork_NoRegister();
	FOOTBALL_API UClass* Z_Construct_UClass_UNeuralNetwork();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_Football();
// End Cross Module References
	void UNeuralNetwork::StaticRegisterNativesUNeuralNetwork()
	{
	}
	UClass* Z_Construct_UClass_UNeuralNetwork_NoRegister()
	{
		return UNeuralNetwork::StaticClass();
	}
	struct Z_Construct_UClass_UNeuralNetwork_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNeuralNetwork_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_Football,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNeuralNetwork_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "NeuralNetwork.h" },
		{ "ModuleRelativePath", "NeuralNetwork.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNeuralNetwork_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNeuralNetwork>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNeuralNetwork_Statics::ClassParams = {
		&UNeuralNetwork::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNeuralNetwork_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNeuralNetwork_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNeuralNetwork()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNeuralNetwork_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNeuralNetwork, 1868936174);
	template<> FOOTBALL_API UClass* StaticClass<UNeuralNetwork>()
	{
		return UNeuralNetwork::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNeuralNetwork(Z_Construct_UClass_UNeuralNetwork, &UNeuralNetwork::StaticClass, TEXT("/Script/Football"), TEXT("UNeuralNetwork"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNeuralNetwork);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
