﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "NeuralNetwork.generated.h"

/**
 * 
 */

UCLASS()
class FOOTBALL_API UNeuralNetwork : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	int layers; // количество слоев в сетке
	UNeuron** neurons; // двумерный массив нейронов
	double*** weights; // трехмерный массив весов. 1 слой, 2 номер нейрона, 3 номер связи нейрона со следующим слоем
	int* size; // колличесвто нейронов в каждом слое

	double sigm_pro(double x); // производная от сигмойдной функции
	

	//void setLayersNotStudy(int n, int* p, string filename) { // производим работу сети без обучения, считывает веса из файла
	//	ifstream fin;
	//	fin.open(filename);
	//	srand(time(0));
	//	layers = n;
	//	neurons = new neuron * [n];
	//	weights = new double** [n - 1];
	//	size = new int[n];
	//	for (int i = 0; i < n; i++) {
	//		size[i] = p[i];
	//		neurons[i] = new neuron[p[i]];
	//		if (i < n - 1) {
	//			weights[i] = new double* [p[i]];
	//			for (int j = 0; j < p[i]; j++) {
	//				weights[i][j] = new double[p[i + 1]];
	//				for (int k = 0; k < p[i + 1]; k++) {
	//					fin >> weights[i][j][k];
	//				}
	//			}
	//		}
	//	}
	//}

	UFUNCTION(BlueprintCallable)
	void setLayers(int n, int* p); // задаем слои, принимаем колличество слоев и колличество нейронов в каждом слое
	
	UFUNCTION(BlueprintCallable)
	void set_input(double p[]); // принмает входные значения

	void ForwardFeeder(int LayerNumber, int start, int stop); // считает значение каждого нейрона

	UFUNCTION(BlueprintCallable)
	double ForwardFeed();

	UFUNCTION(BlueprintCallable)
	void BackPropogation(double prediction, double rresult, double lr); // обратное распространение ошибки

	//bool SaveWeights() { // сохраняем веса
	//	ofstream fout;
	//	fout.open("weights.txt");
	//	for (int i = 0; i < layers; i++) {
	//		if (i < layers - 1) {
	//			for (int j = 0; j < size[i]; j++) {
	//				for (int k = 0; k < size[i + 1]; k++) {
	//					fout << weights[i][j][k] << " ";
	//				}
	//			}
	//		}
	//	}
	//	fout.close();
	//	return 1;
	//}
};

//UCLASS() // нейрон сети
//class FOOTBALL_API UNeuron : public UBlueprintFunctionLibrary
//{
//	GENERATED_BODY()
//
//public:
//	double value; // значение
//	double error; // ошибка
//
//	void act(); // функция активации
//
//};