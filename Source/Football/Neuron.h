﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Neuron.generated.h"

/**
 * 
 */
UCLASS()
class FOOTBALL_API UNeuron : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
		double value; // значение
		double error; // ошибка
	
		void act(); // функция активации
	
};
