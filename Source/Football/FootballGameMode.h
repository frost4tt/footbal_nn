// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FootballGameMode.generated.h"

UCLASS(minimalapi)
class AFootballGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFootballGameMode();
};



