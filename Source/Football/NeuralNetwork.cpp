﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "fstream"
#include "cmath"
#include "random"
#include "time.h"
#include "Neuron.h"
#include "NeuralNetwork.h"

//void UNeuron::act() // функция активации
//{
//	value = (1 / (1 + pow(2.71828, -value)));
//}

double UNeuralNetwork::sigm_pro(double x) // производная от сигмойдной функции
{
	if ((fabs(x - 1) < 1e-9) || (fabs(x) < 1e-9)) return 0.0;
	double res = x * (1.0 - x);
	return res;
}

void UNeuralNetwork::setLayers(int n, int* p) // задаем слои, принимаем колличество слоев и колличество нейронов в каждом слое
{
	srand(time(0));
	layers = n;
	neurons = new UNeuron * [n];
	weights = new double** [n - 1];
	size = new int[n];
	for (int i = 0; i < n; i++) {
		size[i] = p[i];
		neurons[i] = new UNeuron[p[i]];
		if (i < n - 1) {
			weights[i] = new double* [p[i]];
			for (int j = 0; j < p[i]; j++) {
				weights[i][j] = new double[p[i + 1]];
				for (int k = 0; k < p[i + 1]; k++) {
					weights[i][j][k] = ((rand() % 100)) * 0.01 / size[i];
				}
			}
		}
	}
}

void UNeuralNetwork::set_input(double p[]) // принмает входные значения
{
	for (int i = 0; i < size[0]; i++) {
		neurons[0][i].value = p[i];
	}
}

void UNeuralNetwork::ForwardFeeder(int LayerNumber, int start, int stop) // считает значение каждого нейрона
{
	for (int j = start; j < stop; j++) {
		for (int k = 0; k < size[LayerNumber - 1]; k++) {
			neurons[LayerNumber][j].value += neurons[LayerNumber - 1][k].value * weights[LayerNumber - 1][k][j];
		}
		neurons[LayerNumber][j].act();
	}
}

double UNeuralNetwork::ForwardFeed()
{
	for (int i = 1; i < layers; i++) {
		ForwardFeeder(i, 0, size[i]);
	}

	double max = 0; // максимальное значение нейрона
	double prediction = 0; // предсказание сети
	for (int i = 0; i < size[layers - 1]; i++) { // ищем максимальное значение из выходных нейронов и записываем номер нейрона в с максимальным значением
		if (neurons[layers - 1][i].value > max) {
			max = neurons[layers - 1][i].value;
			prediction = i;
		}
	}
	return prediction;
}

void UNeuralNetwork::BackPropogation(double prediction, double rresult, double lr) // обратное распространение ошибки
{
	for (int i = layers - 1; i > 0; i--) {
		if (i == layers - 1) {
			for (int j = 0; j < size[i]; j++) {
				if (j != int(rresult)) {
					neurons[i][j].error = -pow((neurons[i][j].value), 2);
				}
				else {
					neurons[i][j].error = pow(1.0 - neurons[i][j].value, 2);
				}

			}
		}
		else {
			for (int j = 0; j < size[i]; j++) {
				double error = 0.0;
				for (int k = 0; k < size[i + 1]; k++) {
					error += neurons[i + 1][k].error * weights[i][j][k];
				}
				neurons[i][j].error = error;
			}
		}
	}

	for (int i = 0; i < layers - 1; i++) {
		for (int j = 0; j < size[i]; j++) {
			for (int k = 0; k < size[i + 1]; k++) {
				weights[i][j][k] += lr * neurons[i + 1][k].error * sigm_pro(neurons[i + 1][k].value) * neurons[i][j].value;
			}
		}
	}
}