// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FootballCharacter.generated.h"

UCLASS(config=Game)
class AFootballCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AFootballCharacter();

};

